package ru.tsc.avramenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @NotNull
    List<Project> findAll(@NotNull String userId, @NotNull Comparator<Project> comparator);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project findByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Project startById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project startByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project startByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Project finishById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project finishByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project finishByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    Project changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @Nullable
    Project changeStatusByIndex(@NotNull String userId, @NotNull int index, @NotNull Status status);

}
